#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Flask, session, redirect, url_for, escape, request, Response, render_template
import hashlib
import sys
from functools import wraps
import sqlite3
import logging
import os
logging.basicConfig(filename='flask.log',level=logging.DEBUG)
from contextlib import closing

app = Flask(__name__)
app.secret_key = 'w\xf5\x97v\xff3\xccf@\xd6\x9e\xf4k\xfa\xd6\x87\x88C\xaa!\x889l}'


def auth(f):
    ''' 
    '''
    @wraps(f)
    def decorated(*args, **kwargs):
        if not 'kirjautunut' in session:
            return redirect(url_for('kirjautuminen'))
        return f(*args, **kwargs)
    return decorated

@app.route('/')
@auth
def hello_world():
    return "Hello World"

# Tämä funktio tarkistaa onko lomakkeelle syötetty tietoja,
# Tarkistukset tehdään nyt templaten puolella html5:sella
# Sitten se luo tietokantaan uuden Adder-rivin ja ristiviitteen käyttäjän UserAdderiin
# rollbackkiä jos ei onnistu, ettei tule omistamatonta Adder-riviä
@app.route('/createnew', methods=['POST','GET'])
@auth
def createnew():
    if request.method == 'POST':
        name = request.form.get("AdderName")
        b1 = request.form.get("button1")
        b2 = request.form.get("button2")
        b3 = request.form.get("button3")
        b4 = request.form.get("button4")
        try:
            con = sqlite3.connect(os.path.abspath('../../hidden/laskuri'))
        except Exception as e:
            logging.debug("Kanta ei aukea")
            logging.debug(str(e))

        sql = """INSERT INTO Adder (AdderName,b1name,b2name,b3name,b4name) VALUES (:name, :b1, :b2, :b3, :b4)"""
        cur = con.cursor()
        try:
            cur.execute(sql, {"name":name, "b1":b1, "b2":b2, "b3":b3, "b4":b4})
        except:
            con.rollback()
            con.close()
            logging.debug(sys.exc_info()[0])
            return "pieleen meni"

        sql = """SELECT AdderID FROM Adder ORDER BY AdderID DESC LIMIT 1"""
        cur = con.cursor()
        try:
            cur.execute(sql)
        except:
            con.close()
            logging.debug(sys.exc_info()[0])
            return "pieleen meni"

        for row in cur.fetchall():
            viimeinen=row[0]

        sql = """INSERT INTO UserAdder (UserID,AdderID) VALUES (:userid, :adderid)"""
        cur = con.cursor()
        try:
            cur.execute(sql, {"userid":session['id'],"adderid":viimeinen})
            con.commit()
        except:
            con.close()
            logging.debug(sys.exc_info()[0])
            return "pieleen meni"
        return redirect(url_for('etusivu'))

    return render_template('luouusi.html')

# Tämä funktio poistaa UserAdder-taulusta kirjautuneen käyttäjän kohdalta valitun laskennon
# Kun returnissa palataan etusivulle, hakee uusi funktio tietokannasta muuttuneen tiedon
# ja esittää kaikki käyttäjän omistamat laskennot
@app.route('/delete', methods=['POST','GET'])
@auth
def delete():
    try:
        con = sqlite3.connect(os.path.abspath('../../hidden/laskuri'))
    except Exception as e:
        logging.debug("Kanta ei aukea")
        logging.debug(str(e))

    sql = """DELETE FROM UserAdder WHERE AdderID = :adderID AND UserID = :userID"""

    cur = con.cursor()
    try:
        cur.execute(sql, {"adderID":session['valittuID'], "userID":session['id']})
        con.commit()
    except:
        con.close()
        logging.debug(sys.exc_info()[0])
        return "pieleen meni"

    con.close()
    return redirect(url_for('etusivu'))

# Tämä funkio esittää valitun laskennon kaikki tulokset
@app.route('/count', methods=['POST','GET'])
@auth
def count():
    valittuID = request.args.get('adderid')
    session['valittuID'] = valittuID

    try:
        con = sqlite3.connect(os.path.abspath('../../hidden/laskuri'))
    except Exception as e:
        logging.debug("Kanta ei aukea")
        logging.debug(str(e))

    sql = """SELECT * FROM Counts WHERE AdderID = :adderID"""

    cur = con.cursor()
    try:
        cur.execute(sql, {"adderID":valittuID})
    except:
        con.close()
        logging.debug(sys.exc_info()[0])
        return "pieleen meni"

    tiedot = []
    for row in cur.fetchall():
        rivi = {'ButtonName': row[1], 'CreationTime': row[2]}
        tiedot.append( rivi )

    con.close()
    return render_template('laskento.html', tiedot=tiedot)

# Tämä funktio listaa kaikki laitteen tiedot.
# Jinjan puolella esitetään select-option-tyyppisesti kaikki käyttäjän omistamat laskennot
# Valitessaan jonkin uuden laskennon, ja painaessaan Päivitä, UPDATEtaan tietokantaan aktiivinen
@app.route('/device', methods=['POST','GET'])
@auth
def device():
    uusiAktiivinen = request.form.get('selectedAdder')

    if uusiAktiivinen:
        try:
            con = sqlite3.connect(os.path.abspath('../../hidden/laskuri'))
        except Exception as e:
            logging.debug("Kanta ei aukea")
            logging.debug(str(e))

        sql = """UPDATE Device SET AdderID = :uusiAktiivinen WHERE DeviceID = :deviceID"""

        cur = con.cursor()
        try:
            cur.execute(sql, {"uusiAktiivinen":uusiAktiivinen, "deviceID":session['valittuLaite']})
            con.commit()
        except:
            con.close()
            logging.debug(sys.exc_info()[0])
            return "pieleen meni"

        con.close()
        return redirect(url_for('etusivu'))

    laiteID = int(request.args.get('deviceid', 0))
    session['valittuLaite'] = laiteID
    return render_template('laite.html', deviceid=laiteID, valittu=uusiAktiivinen)
    

# Tämä funktio tarkistaa käyttäjän syöttämän tunnuksen ja salasanan tietokannasta
# Lisäksi tämä funktio hoitaa autentikoinnin: jos ok, luodaan sessio, jos ei tullaan takaisin tälle sivulle
# Jos tunnus-salasana ok: lisätään sessioon käyttäjä id(tarvitaan myöhemmin) ja tunnus(huvikseen)
@app.route('/kirjautuminen', methods=['POST','GET']) 
def kirjautuminen():
    m = hashlib.sha512()
    username = request.form.get("kayttajatunnus", "0")
    password = request.form.get("salasana", "0")
    m.update(password)
    salasana = m.hexdigest()

    try:
        con = sqlite3.connect(os.path.abspath('../../hidden/laskuri'))
    except Exception as e:
        logging.debug("Kanta ei aukea")
        logging.debug(str(e))

    sql = """SELECT * FROM User WHERE Username = :name AND Password = :passw"""

    cur = con.cursor()
    try:
        cur.execute(sql, {"name":username, "passw":salasana})
    except:
        logging.debug(sys.exc_info()[0])
        tieto = "pieleen meni"
        return render_template('kirjautuminen.html', teksti=tieto)

    tunnus = []
    for row in cur.fetchall():
        tunnus.append(row[0])
        tunnus.append( row[2] )

    if tunnus:
        session['kirjautunut'] = "login"
        con.close()
        session['tunnus'] = tunnus[0]
        session['id'] = tunnus[1]
        return redirect(url_for('etusivu'))

    logging.debug(sys.exc_info()[0])
    con.close()
    return render_template('kirjautuminen.html')

# Tänne tullaan vain kirjautuneena
# Ensin verrataan käyttäjän id:tä UserAdderiin, eli mitä Addereita tämä käyttäjä omistaa
# Sama ristiinvertailu tehdään sitten käyttäjän Deviceille
# Lopuksi laitetaan tietokannasta kaivettuja juttuja sessionvariableihin
@app.route('/etusivu', methods=['POST','GET'])
@auth
def etusivu():
    try:
        con = sqlite3.connect(os.path.abspath('../../hidden/laskuri'))
    except Exception as e:
        logging.debug("Kanta ei aukea")
        logging.debug(str(e))

    sql = """SELECT * FROM Adder INNER JOIN UserAdder ON Adder.AdderID = UserAdder.AdderID WHERE UserAdder.UserID = :userid"""

    cur = con.cursor()

    try:
        cur.execute(sql, {"userid":session['id']})
    except:
        logging.debug(sys.exc_info()[0])
        tieto = "pieleen meni"
        con.close()
        return render_template('etusivu.html', laskennot=4)

    adders = []
    ownedadders =[]
    for row in cur.fetchall():
        tiedot = {'Name': row[0], 'Button1': row[1], 'Button2': row[2], 'Button3': row[3], 'Button4': row[4], 'AdderID': row[5] }
        adders.append( tiedot )
        ownedadders.append(row[5])

    session['ownedadders'] = ownedadders

    sql = """SELECT * FROM Device INNER JOIN UserDevice ON Device.DeviceID = UserDevice.DeviceID WHERE UserDevice.UserID = :userid"""

    cur = con.cursor()

    try:
        cur.execute(sql, {"userid":session['id']})
    except:
        logging.debug(sys.exc_info()[0])
        tieto = "pieleen meni"
        con.close()
        return render_template('etusivu.html', laskennot=4)

    devices = []
    owneddevices = []
    activeondevice = []

    for row in cur.fetchall():
        laite = {'Name': row[0], 'Description': row[1], 'DeviceID': row[3], 'AdderID': row[4]}
        devices.append( laite )
        owneddevices.append(row[3])
        active = {'deviceid': row[3], 'adderid': row[4]}
        activeondevice.append(active)

    session['owneddevices'] = owneddevices
    session['activeondevice'] = activeondevice
    session['devices'] = devices
    session['adders'] = adders
    
    if adders or devices:
        con.close()
        return render_template('etusivu.html', laskennot=adders, laitteet=devices)

    con.close()
    return render_template('etusivu.html')

# Kirjaudutaan ulos, eli popataan session['kirjautunut'], ja näin ollen ei pääse enää muille sivuille
# Popataan myös kaikki muut session aikana tallennetut variablet
@app.route('/logout', methods=['POST','GET']) 
def logout():
    session.pop('kirjautunut', None)
    session.pop('ownedadders', None)
    session.pop('tunnus', None)
    session.pop('id', None)
    session.pop('owneddevices', None)
    session.pop('activeondevice', None)
    session.pop('devices', None)
    session.pop('adders', None)
    return redirect(url_for('kirjautuminen'))

    
if __name__ == '__main__':
    app.debug = True
    app.run(debug=True)





