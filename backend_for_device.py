#!/usr/bin/python
# -*- coding: utf-8 -*-

from flask import Flask, session, redirect, url_for, escape, request, Response, render_template, json
import hashlib
from functools import wraps
import sqlite3
import logging
import os
import sys

logging.basicConfig(filename='flask.log',level=logging.DEBUG)

app = Flask(__name__)

app.secret_key = '\x97\n\x1fDI\xceE\xdf\x90\x1e\x02\x8dUg~\xb1^M\xdb\xee&:\x7fF'

def auth(f):
    '''
    '''
    @wraps(f)
    def decorated(*args, **kwargs):
        if not 'login' in session:
            return redirect(url_for('kirjaudu'))
        return f(*args, **kwargs)
    return decorated

# Checks what is the active count on the logged in device
# returns 32-char long String that includes pipes in the middle
# Also places buttonNames and AdderID into session-values
def fetchActiveToDisplay():
    try:
        con = sqlite3.connect(os.path.abspath('../../hidden/laskuri'))
        con.row_factory = sqlite3.Row
    except:
        logging.debug("Virhetta tulee fetchactivesta")

    # Search for the button names to whatever is active on logged in device
    sql = """
    SELECT b1name, b2name, b3name, b4name, AdderID
    FROM Adder
    WHERE Adder.AdderID IN
    (SELECT AdderID FROM Device WHERE DeviceID = :deviceid) ;
    """

    cur = con.cursor()

    try:
        cur.execute(sql, {"deviceid":session["login"]})
    except:
        logging.debug(sys.exc_info()[0])
        con.close()
        return "Something absolutely horrible happened! Learn SQL!"

    devices = []

    for row in cur.fetchall():
        devices.append( row[0] )
        devices.append( row[1] )
        devices.append( row[2] )
        devices.append( row[3] )
        session['active'] = row[4]

    # parse the response to JSON-format string
    # and pass button names to session variables
    palautus = "{"
    kierros = 0
    for i in devices:
        button = "b" + str(kierros)
        session[button] = str(devices[kierros])
        if (kierros < 3):
            palautus = palautus + '"' + button + '": "' + i + '", '
        else:
            palautus = palautus + '"' + button + '": "' + i + '"}'
        kierros = kierros + 1

    return palautus


# If logged in, checks the request and inserts into database
# In the end, checks what is active and pushes that to the display
@app.route('/device', methods=['GET'])
@auth
def device():
    try:
        luku = str(request.args['luku'])
    except:
        luku = "ei"
        return 'tuli jotain epalukua'

    try:
        con = sqlite3.connect(os.path.abspath('../../hidden/laskuri'))
        con.row_factory = sqlite3.Row
    except:
        logging.debug("Tänne mitä haluaa lokiin kirjoittaa")

    sql = """
    INSERT INTO Counts(ButtonName,AdderID) VALUES (:button, :adder);
    """

    cur = con.cursor()

    try:
        cur.execute(sql, {"button":session[luku], "adder":session["active"]})
        con.commit()
    except:
        logging.debug(sys.exc_info()[0])
        con.close()
        return "Something happened!" + luku

    con.close()
    return fetchActiveToDisplay()


# Checks from database, if there exists the login-keyid.
# Returns message, that says whether ok or not
@app.route('/kirjaudu', methods=['POST','GET'])
def kirjaudu():
    try:
        keyid = request.form['keyid']
    except:
        keyid = ""

    # prepare the database connection and set it to rows
    try:
    	con = sqlite3.connect(os.path.abspath('../../hidden/laskuri'))
    	con.row_factory = sqlite3.Row
    except:
    	logging.debug("Tänne mitä haluaa lokiin kirjoittaa")

    sql = """
    SELECT DeviceID FROM Device WHERE KeyID = :keyid ;
    """

    cur = con.cursor()

    # hash the posted keyid
    m = hashlib.sha512()
    m.update(keyid)
    # use hexdigest for easier comparasion
    tunnus = m.hexdigest()

    # search the database for posted keyid
    try:
        cur.execute(sql, {"keyid":tunnus})
    except:
    	logging.debug(sys.exc_info()[0])
        con.close()
        return "Something absolutely horrible happened! Learn SQL!"

    devices = []

    for row in cur.fetchall():
        devices.append( row[0] )

    # if something is put to the list of devices, then set login to deviceid and start session
    if devices:
        session['login'] = str(devices[0])
        con.close()
        return fetchActiveToDisplay()
    
    # if keyid is not found, close connection and return message
    con.close()
    msg = "You entered bad keyid! No login."
    return msg


# Ends session
@app.route('/logout', methods=['POST','GET']) 
def logout():
    # clear the session-variable login, thus ending session
    session.pop('login',None)
    return "Logout completed"


if __name__ == '__main__':
    app.debug = True
    app.run(debug=True)


