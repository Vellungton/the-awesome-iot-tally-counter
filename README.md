# The awesome IoT tally counter #

With IoT tally counter all your tally counter problems will be solved!

Tired of having the same single button under the thumb?

IoT tally counter gives you four buttons that are all fully customizable! Also you get full access to the Web-UI
and can create as many new counts as you wish! Just log in, create new count table, and turn on the device.
New countable items will now be displayed on your device, and every time you push a button (count), everything is stored securily
to the cloud. View and manage your counts as you wish with the web-UI.

Once you try IoT tally counter, you never go back to the mechanical ones!

;)

This repository contains some of my code for building very simple fullstack IoT-system from the cheap components that I got lying around.  Making this was essentially a learning process, but more importantly it was fun! Basically tally counter goes IoT has four main components.

1. The device is build from Arduino Uno and a BLE-shield by Adafruit.
2. The gateway is a Python code that uses the Adafruit library for the BLE-shield.
3. The backend for the device is Python/Flask flavoured with Sqlite
4. The backend for the UI is yet another Flask that's connected to the same Sqlite db.

-------

As you can clearly see, things are under construction and ugly. The most urgents things to do:

- Someone really needs to make the frontend!
- True JSON operability, getting rid of String parsing.
- https instead of http
- sleep-mode and power savings mode for the Arduino
- a whole lot..